import XCTest
@testable import NewsReader

class NetworkServiceTests: XCTestCase {

    private let subject = NetworkServiceMock()

    override func setUp() {
        subject.dataToReturn = nil
    }

    func testNetworkServiceReturnsArticles() {
        let data = DataLoader.data(forResource: "Articles", ofType: "json")
        let topHeadLines = TopHeadLines(params: [:])

        subject.dataToReturn = data
        subject.load(resource: topHeadLines) { (result: Result<News>) in
            if case .success(let value) = result {
                if let arcticles = value {
                    XCTAssertNotNil(arcticles)
                    XCTAssert(arcticles.count == 1)
                } else {
                    XCTAssert(false)
                }
            }
        }
    }

    func testNetworkServiceReturnsParameterMissingError() {
        let topHeadLines = TopHeadLines(params: [:])
        let data = DataLoader.data(forResource: "ParameterMissingError", ofType: "json")
        let response = HTTPURLResponseMock(code: 400)!

        let expectation = XCTestExpectation(description: "")

        subject.dataToReturn = data
        subject.responseToReturn = response
        subject.load(resource: topHeadLines) { (result: Result<News>) in
            guard case .error(let value) = result else {
                XCTAssert(false)
                return
            }

            guard let error = value as? NetworkServiceError else {
                XCTAssert(false, "Error should be a type of NetworkServiceError")
                return
            }

            XCTAssertEqual(error.code, "parametersMissing")
            XCTAssertEqual(error.status, "error")
            XCTAssertEqual(error.message, "Required parameters are missing. Please set any of the following parameters and try again: sources, q, language, country, category.")

            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1)
    }

    func testNetworkServiceReturnsDataIsEmptyError() {
        let topHeadLines = TopHeadLines(params: [:])
        let response = HTTPURLResponseMock(code: 200)!

        let expectation = XCTestExpectation(description: "")

        subject.dataToReturn = nil
        subject.responseToReturn = response
        subject.load(resource: topHeadLines) { (result: Result<News>) in
            guard case .error(let value) = result else {
                XCTAssert(false)
                return
            }

            guard let error = value as? NetworkServiceError else {
                XCTAssert(false, "Error should be a type of NetworkServiceError")
                return
            }

            XCTAssertEqual(error.code, "General Error")
            XCTAssertEqual(error.message, "Data is empty")

            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1)
    }

    func testNetworkServiceReturnsResponseIsInvalidError() {
        let topHeadLines = TopHeadLines(params: [:])

        let expectation = XCTestExpectation(description: "")

        subject.responseToReturn = nil
        subject.load(resource: topHeadLines) { (result: Result<News>) in
            guard case .error(let value) = result else {
                XCTAssert(false)
                return
            }

            guard let error = value as? NetworkServiceError else {
                XCTAssert(false, "Error should be a type of NetworkServiceError")
                return
            }

            XCTAssertEqual(error.code, "General Error")
            XCTAssertEqual(error.message, "Response is invalid")

            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1)
    }

    func testNetworkServiceReturnsErrorOnEmptyJSON() {
        let topHeadLines = TopHeadLines(params: [:])
        let data = DataLoader.data(forResource: "Empty", ofType: "json")

        let expectation = XCTestExpectation(description: "")

        subject.dataToReturn = data
        subject.load(resource: topHeadLines) { (result: Result<News>) in
            guard case .error(let value) = result else {
                XCTAssert(false)
                return
            }

            guard let error = value as? NetworkServiceError else {
                XCTAssert(false, "Error should be a type of NetworkServiceError")
                return
            }

            XCTAssertEqual(error.code, "General Error")
            XCTAssertEqual(error.message, "Response is invalid")

            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 1)
    }

}
