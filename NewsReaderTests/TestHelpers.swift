import Foundation

struct DataLoader {
    static func data(forResource resource: String, ofType type: String) -> Data {
        let bundle = Bundle(for: NewsReaderTests.self)

        guard let path = bundle.path(forResource: resource, ofType: type) else {
            fatalError("Couldn't create path for file:\(resource) of type: \(type)")
        }

        let jsonURL = URL(fileURLWithPath: path)
        guard let data = try? Data(contentsOf: jsonURL) else {
            fatalError("Coulnd't load data at path: \(path)")
        }

        return data
    }
}
