import XCTest
@testable import NewsReader

final class NewsFeedInteractorMockOutput: NewsFeedInteractorOutput {

    var didLoadNewsCalled = false
    func didLoadNews(_ news: [ArticleViewModel]) {
        didLoadNewsCalled = true
    }

    var failedLoadNews = false
    func failedLoadNews(_ error: Error) {
        failedLoadNews = true
    }
}

class NewsFeedInteractorTests: XCTestCase {

    private var subject: NewsFeedInteractorInput!
    private var output: NewsFeedInteractorMockOutput!
    private var networkService: NetworkServiceMock!

    override func setUp() {
        networkService = NetworkServiceMock()
        output = NewsFeedInteractorMockOutput()

        subject = NewsFeedInteractor(networkService: networkService)
        subject.output = output
    }

    override func tearDown() {
        networkService = nil
        output = nil
        subject = nil
    }

    func testSubjectCallsDidLoadNews() {
        networkService.dataToReturn = DataLoader.data(forResource: "Articles", ofType: "json")
        networkService.responseToReturn = HTTPURLResponseMock(code: 200)!

        subject.loadNews(fromCategory: .business)
        XCTAssertTrue(output.didLoadNewsCalled)
        XCTAssertFalse(output.failedLoadNews)
    }

    func testSubjectCallsFailedLoadNews() {
        networkService.dataToReturn = nil

        subject.loadNews(fromCategory: .business)
        XCTAssertTrue(output.failedLoadNews)
        XCTAssertFalse(output.didLoadNewsCalled)
    }

    func testSubjectCallsHasNewPageIsTrue() {
        networkService.dataToReturn = DataLoader.data(forResource: "Articles", ofType: "json")
        networkService.responseToReturn = HTTPURLResponseMock(code: 200)!

        subject.loadNews(fromCategory: .business)
        XCTAssertTrue(subject.hasNextPage)
    }

    func testSubjectCallsHasNewPageIsFalseWhenNoNewsDownloaded() {
        subject.loadNews(fromCategory: .business)
        XCTAssertFalse(subject.hasNextPage)
    }

    func testSubjectCallsDidLoadNewsWhenDownloadNextPage() {
        networkService.dataToReturn = DataLoader.data(forResource: "Articles", ofType: "json")
        networkService.responseToReturn = HTTPURLResponseMock(code: 200)!

        subject.loadNews(fromCategory: .business)
        subject.loadNextPage(fromCategory: .business)

        XCTAssertTrue(output.didLoadNewsCalled)
        XCTAssertFalse(output.failedLoadNews)
    }

}
