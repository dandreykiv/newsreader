import XCTest
@testable import NewsReader

class NewsReaderTests: XCTestCase {
    func testArticlesParsing() {
        let data = DataLoader.data(forResource: "Articles", ofType: "json")

        let articles = News.parse(data: data)!
        XCTAssertTrue(articles.totalResults == 20, "Not equeal")
        XCTAssertTrue(articles.status == "ok", "Not equeal")
        XCTAssertTrue(articles.count == 1, "Not equeal")

        let article = articles[0]!
        XCTAssertEqual(article.title!, "Cocu na zoveelste nederlaag...")
    }
}
