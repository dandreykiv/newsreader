import Foundation

final class NetworkServiceMock: NetworkService {
    var dataToReturn: Data? = nil
    var errorToReturn: Error? = nil
    var responseToReturn: URLResponse? = nil

    let urlSession: URLSessionMock

    init() {
        urlSession = URLSessionMock()
        super.init(
            configuration: NetworkConfiguration.defaultConfiguration(),
            urlSession: urlSession
        )
    }

    override func load<T>(resource: Resource, completion: @escaping (Result<T>) -> Void) where T : Parsable {
        urlSession.dataToReturn = dataToReturn
        urlSession.errorToReturn = errorToReturn
        urlSession.responseToReturn = responseToReturn

        super.load(resource: resource, completion: completion)
    }
}
