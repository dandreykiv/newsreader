import Foundation

final class URLSessionDataTaskMock: URLSessionDataTask {
    let taskCompletionHandler: () -> Void

    init(completionHandler: @escaping () -> Void) {
        taskCompletionHandler = completionHandler
    }

    override func resume() {
        taskCompletionHandler()
    }
}

