import Foundation

final class HTTPURLResponseMock: HTTPURLResponse {
    private let code: Int

    init?(code: Int) {
        self.code = code
        super.init(url: URL(string: "https://example.com")!, statusCode: code, httpVersion: nil, headerFields: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
