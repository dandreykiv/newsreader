import Foundation

protocol Resource {
    var endpoint: Endpoint { get }
    var params: [String: Any] { get }
}

struct TopHeadLines: Resource {
    let endpoint: Endpoint
    let params: [String : Any]

    init(params: [String : Any]) {
        endpoint = .topHeadlines
        self.params = params
    }
}
