import Foundation

enum Endpoint: String {
    case topHeadlines
    case everything
    case sources

    var path: String {
        switch self {
        case .topHeadlines:
            return apiVersion + "/top-headlines"
        case .everything:
            return apiVersion + "/everything"
        case .sources:
            return apiVersion + "/sources"
        }
    }

    var apiVersion: String {
        return "/v2"
    }
}
