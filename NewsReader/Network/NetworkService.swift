import Alamofire

class NetworkService {
    private let configuration: NetworkConfiguration
    private let urlSession: URLSessionProtocol

    convenience init() {
        let urlSession = URLSession(
            configuration: URLSessionConfiguration.default,
            delegate: nil,
            delegateQueue: OperationQueue.main
        )

        self.init(
            configuration: NetworkConfiguration.configuration(),
            urlSession: urlSession
        )
    }

    init(configuration: NetworkConfiguration, urlSession: URLSessionProtocol) {
        self.configuration = configuration
        self.urlSession = urlSession
    }

    private func requestUrl(from resource: Resource) -> URL? {
        var components = URLComponents(string: configuration.baseUrl)

        components?.path = resource.endpoint.path
        components?.queryItems = resource.params.map { (pair) -> URLQueryItem in
            return URLQueryItem(name: pair.key, value: "\(pair.value)")
        }

        return components?.url
    }

    func load<T: Parsable>(resource: Resource, completion: @escaping (Result<T>) -> Void) {
        guard let url = requestUrl(from: resource) else {
            return DispatchQueue.main.async {
                completion(.error(NetworkServiceError(code: "General error", message: "Couldn't create url")))
            }
        }

        var request = URLRequest(url: url)
        request.addValue(configuration.apiKey, forHTTPHeaderField: "X-Api-Key")

        let task = urlSession.dataTask(with: request) { (data, response, error) in
            guard let response = response as? HTTPURLResponse else {
                return completion(
                    .error(NetworkServiceError(code: "General Error", message: "Response is invalid"))
                )
            }

            guard let data = data else {
                return completion(
                    .error(NetworkServiceError(code: "General Error", message: "Data is empty"))
                )
            }

            guard response.statusCode == 200 else  {
                let error = NetworkServiceError.parse(data: data)
                return completion(.error(error))
            }

            completion(.success(T.parse(data: data)))
        }

        task.resume()
    }
}
