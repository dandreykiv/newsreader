import Foundation

protocol Parsable {
    static func parse(data: Data) -> Self?
}

enum Result<T: Parsable> {
    case error(_ value: Error?)
    case success(_ value: T?)
}

