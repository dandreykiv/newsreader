import Foundation

struct NetworkConfiguration: Codable {
    let baseUrl: String
    let apiKey: String
}

extension NetworkConfiguration {
    static func defaultConfiguration() -> NetworkConfiguration {
        return NetworkConfiguration(baseUrl: "", apiKey: "")
    }

    static func configuration() -> NetworkConfiguration {
        guard let url = Bundle.main.url(forResource: "NetworkConfiguration", withExtension: "plist") else {
            fatalError("Couldn't find network configuration file")
        }

        do {
            let data = try Data(contentsOf: url)
            let decoder = PropertyListDecoder()

            return try decoder.decode(NetworkConfiguration.self, from: data)
        } catch {
            fatalError("Couldn't read property file")
        }

    }
}
