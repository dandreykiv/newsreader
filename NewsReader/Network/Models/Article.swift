import Foundation

struct Article {
    struct Source {
        let id: String?
        let name: String?
    }

    let source: Source
    let author: String?
    let title: String?
    let description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt: Date?
    let content: String?
}

extension Article: Codable {}
extension Article.Source: Codable {}
