import Foundation

struct News {
    let status: String
    let totalResults: Int
    private let articles: [Article]
}

extension News {
    var count: Int {
        return articles.count
    }

    var all: [Article] {
        return articles
    }

    subscript(_ index: Int) -> Article? {
        get {
            guard index >= 0, index < articles.count else {
                return nil
            }

            return articles[index]
        }
    }
}

extension News: Codable {}

extension News: Parsable {
    static func parse(data: Data) -> News? {
        let decoder = JSONDecoder()
        do {
            decoder.dateDecodingStrategy = .iso8601
            return try decoder.decode(News.self, from: data)
        } catch {
            print(error)
        }

        return nil
    }
}
