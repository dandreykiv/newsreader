import Foundation

struct NetworkServiceError: Error {
    let status: String?
    let code: String?
    let message: String?

    init(code: String, message: String) {
        self.code = code
        self.message = message
        self.status = "error"
    }
}

extension NetworkServiceError: Codable {}

extension NetworkServiceError: Parsable {
    static func parse(data: Data) -> NetworkServiceError? {
        let decoder = JSONDecoder()
        do {
            return try decoder.decode(NetworkServiceError.self, from: data)
        } catch {
            print(error)
        }

        return nil
    }
}
