import Foundation

enum AccessibilityIdentifiers {
    static let generalTabIdentifier = "general"
    static let businessTabIdentifier = "business"
    static let entertainmentTabIdentifier = "entertainment"
    static let scienceTabIdentifier = "science"
    static let sportsTabIdentifier = "sports"
}
