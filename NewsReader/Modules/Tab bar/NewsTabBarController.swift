import UIKit

class NewsTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        viewControllers = [
            newsViewController(forCategory: .general),
            newsViewController(forCategory: .business),
            newsViewController(forCategory: .entertainment),
            newsViewController(forCategory: .science),
            newsViewController(forCategory: .sports),
        ]
    }

    private func newsViewController(forCategory category: Category) -> UIViewController {
        let feedViewController = NewsFeedViewController(
            interactor: NewsFeedInteractor(networkService: NetworkService()),
            imageDownloader: TableViewImageDownloader(),
            category: category
        )

        let title = category.name.capitalized

        feedViewController.tabBarItem.title = title
        feedViewController.tabBarItem.image = UIImage(named: category.name)
        feedViewController.tabBarItem.accessibilityIdentifier = category.accessibilityIdentifier
        feedViewController.title = title

        let navigationController = UINavigationController(rootViewController: feedViewController)

        return navigationController
    }
}

private extension Category {
    var accessibilityIdentifier: String? {
        switch self {
        case .general:
            return AccessibilityIdentifiers.generalTabIdentifier
        case .business:
            return  AccessibilityIdentifiers.businessTabIdentifier
        case .entertainment:
            return AccessibilityIdentifiers.entertainmentTabIdentifier
        case .science:
            return AccessibilityIdentifiers.scienceTabIdentifier
        case .sports:
            return AccessibilityIdentifiers.sportsTabIdentifier

        default:
            return nil
        }
    }
}
