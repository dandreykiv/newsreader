import UIKit
import WebKit

class NewsContentViewController: UIViewController {

    private var webView: WKWebView!
    private let articleUrl: URL

    init(url: URL) {
        articleUrl = url

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)

        view = webView
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.custom("F1F1F1")
        navigationItem.largeTitleDisplayMode = .never

        let request = URLRequest(url: articleUrl)
        webView.load(request)
    }
}
