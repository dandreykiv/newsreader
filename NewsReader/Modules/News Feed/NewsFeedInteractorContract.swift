import Foundation

protocol NewsFeedInteractorInput: class {
    var output: NewsFeedInteractorOutput? { get set }
    var hasNextPage: Bool { get }

    func loadNews(fromCategory category: Category)
    func loadNextPage(fromCategory category: Category)
}

protocol NewsFeedInteractorOutput: class {
    func didLoadNews(_ news: [ArticleViewModel])
    func failedLoadNews(_ error: Error)
}
