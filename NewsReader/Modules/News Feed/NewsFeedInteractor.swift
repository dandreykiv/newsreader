import Foundation

final class NewsFeedInteractor: NewsFeedInteractorInput {

    private let networkService: NetworkService
    private let pageSize = 20
    private var totalResults = 0
    private var page = 0

    private var numberOfPages: Int {
        let pages = totalResults / pageSize
        return totalResults % pageSize == 0 ? pages : pages + 1
    }

    private var news: [Article] = []

    weak var output: NewsFeedInteractorOutput?

    private var nextPage: Int {
        if news.isEmpty {
            return page
        }

        if news.count == totalResults {
            return page
        }

        return page + 1
    }

    var hasNextPage: Bool {
        return news.count < totalResults
    }

    init(networkService: NetworkService) {
        self.networkService = networkService
    }

    func loadNews(fromCategory category: Category) {
        let params: [String: Any] = [
            "category": category.name,
            "page": page,
            "country": "us",
            "pageSize": pageSize
        ]

        let resource = TopHeadLines(params: params)

        networkService.load(resource: resource) { [weak self] (result: Result<News>) in
            switch result {
            case .error(let value):
                guard let error = value else {
                    fatalError("Couldn't retrieve error")
                }
                self?.output?.failedLoadNews(error)
            case .success(let value):
                guard let news = value else {
                    fatalError("Could't retrive articles")
                }
                self?.totalResults = news.totalResults
                self?.news.append(contentsOf: news.all)

                let models = self?.news.map(ArticleViewModel.init)
                self?.output?.didLoadNews(models!)
            }
        }
    }

    func loadNextPage(fromCategory category: Category) {
        guard hasNextPage else { return }

        page = nextPage
        loadNews(fromCategory: category)
    }
}
