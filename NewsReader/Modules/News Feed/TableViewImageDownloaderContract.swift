import UIKit

protocol ImageDownloaderType: class {
    var output: TableViewImageDownloaderOutuput? { get set }

    func downloadImage(url: URL?, forCellAt indexPath: IndexPath)
    func image(forUrl url: URL?) -> UIImage?
}

protocol TableViewImageDownloaderOutuput: class {
    func downloaded(_ image: UIImage, forCellAt indexPaths: Set<IndexPath>)
}
