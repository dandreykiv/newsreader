import Foundation

enum Category: String {
    case business
    case entertainment
    case general
    case health
    case science
    case sports
    case technology

    var name: String {
        return self.rawValue
    }
}
