import AlamofireImage

/// This is a helper storage which keeps list of indexPaths
/// associated with image url.
/// In most cases the relation will be 1:1 (URL -> IndexPath)
private struct IndexPathMap {

    /// Some news, potentially, might have the same image, like placehorders.
    /// Thus 1:1 (IndexPath -> URL) relation is not correct in all cases.
    /// The ralation 1:[1...] (URL: [IndexPath]) is more preferable.
    private var urlMap: [URL: Set<IndexPath>] = [:]

    mutating func associate(indexPath: IndexPath, with url: URL) {
        var indexes = urlMap[url] ?? Set()
        indexes.insert(indexPath)

        urlMap[url] = indexes
    }

    func indexes(for url: URL) -> Set<IndexPath> {
        guard let indexes = urlMap[url] else {
            return []
        }

        return indexes
    }
}

class TableViewImageDownloader: ImageDownloaderType {

    weak var output: TableViewImageDownloaderOutuput?
    private let downloader: ImageDownloader
    private var indexPathMap: IndexPathMap

    init() {
        downloader = ImageDownloader(
            configuration: ImageDownloader.defaultURLSessionConfiguration(),
            downloadPrioritization: .fifo,
            maximumActiveDownloads: 6,
            imageCache: AutoPurgingImageCache()
        )

        indexPathMap = IndexPathMap()
    }

    func downloadImage(url: URL?, forCellAt indexPath: IndexPath) {
        guard let url = url else { return }

        indexPathMap.associate(indexPath: indexPath, with: url)

        let request = URLRequest(url: url)

        downloader.download(request) { [weak self] response in
            guard let image = response.result.value else { return }
            guard let originalUrl = response.request?.url else { return }
            guard let indexes = self?.indexPathMap.indexes(for: originalUrl) else { return }

            self?.output?.downloaded(image, forCellAt: indexes)
        }
    }

    func image(forUrl url: URL?) -> UIImage? {
        guard let url = url else { return nil }
        return downloader.imageCache?.image(for: URLRequest(url: url), withIdentifier: nil)
    }
}
