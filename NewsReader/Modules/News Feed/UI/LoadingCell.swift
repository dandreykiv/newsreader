import UIKit

class LoadingCell: UITableViewCell {
    @IBOutlet private weak var spinner: UIActivityIndicatorView!

    func startAnimating() {
        spinner.startAnimating()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        spinner.stopAnimating()
    }
}
