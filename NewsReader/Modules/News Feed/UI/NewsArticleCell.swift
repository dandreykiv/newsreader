import UIKit

class NewsArticleCell: UITableViewCell {
    @IBOutlet private weak var articleImage: UIImageView!
    @IBOutlet private weak var title: UILabel!
    @IBOutlet private weak var publishedAt: UILabel!
}

extension NewsArticleCell {
    func configure(with model: ArticleViewModel) {
        title.text = model.title
        publishedAt.text = model.publishedAt
    }

    func upateImage(_ image: UIImage?) {
        UIView.transition(with: articleImage,
                          duration: 0.4,
                          options: .transitionCrossDissolve,
                          animations: { self.articleImage.image = image },
                          completion: nil)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        title.text = nil
        publishedAt.text = nil
        articleImage.image = nil
    }
}
