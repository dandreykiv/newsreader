import UIKit

class NewsFeedViewController: UITableViewController {
    private let interactor: NewsFeedInteractorInput
    private let imageDownloader: ImageDownloaderType

    private let category: Category
    private var news: [ArticleViewModel]
    private weak var alertController: UIAlertController?

    init(interactor: NewsFeedInteractorInput, imageDownloader: ImageDownloaderType, category: Category) {
        self.interactor = interactor
        self.imageDownloader = imageDownloader
        self.category = category
        self.news = []

        super.init(nibName: nil, bundle: nil)
        self.interactor.output = self
        self.imageDownloader.output = self
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.custom("F1F1F1")
        
        configureTableView()

        interactor.loadNews(fromCategory: category)
    }

    private func configureTableView() {
        tableView.separatorStyle = .none
        register(cell: LoadingCell.self)
        register(cell: NewsArticleCell.self)

        tableView.reloadData()
    }

    private func register(cell: Reusable.Type) {
        let nib = UINib(nibName: cell.nibName, bundle: Bundle.main)
        tableView.register(nib, forCellReuseIdentifier: cell.identifier)
    }
}

extension NewsFeedViewController: NewsFeedInteractorOutput {
    func didLoadNews(_ news: [ArticleViewModel]) {
        self.news = news

        tableView.reloadData()
    }

    func failedLoadNews(_ error: Error) {
        guard self.alertController == nil else { return }

        let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

        present(alertController, animated: true)
        self.alertController = alertController
    }
}

// MARK: UITableViewDatasource Methods
extension NewsFeedViewController {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < news.count {
            return tableView.dequeueReusableCell(withIdentifier: NewsArticleCell.identifier, for: indexPath)
        }

        return tableView.dequeueReusableCell(withIdentifier: LoadingCell.identifier, for: indexPath)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (interactor.hasNextPage || news.isEmpty) {
            return news.count + 1
        }

        return news.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row < news.count) ? 120 : 60
    }
}

// MARK: UITableViewDelegate Methods
extension NewsFeedViewController {
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? LoadingCell {
            handleLoadingCell(cell)
        }

        if let cell = cell as? NewsArticleCell {
            handleArticleCell(cell, at: indexPath)
        }
    }

    private func handleLoadingCell(_ cell: LoadingCell) {
        cell.startAnimating()
        interactor.loadNextPage(fromCategory: category)
    }

    private func handleArticleCell(_ cell: NewsArticleCell, at indexPath: IndexPath) {
        let model = news[indexPath.row]
        cell.configure(with: model)

        guard let imageUrl = model.imageUrl else {
            return
        }

        guard let image = imageDownloader.image(forUrl: imageUrl) else {
            return imageDownloader.downloadImage(url: imageUrl, forCellAt: indexPath)
        }

        cell.upateImage(image)
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.row < news.count else {
            return
        }

        let article = news[indexPath.row]
        guard let url = article.articleUrl else {
            return
        }

        let controller = NewsContentViewController(url: url)
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension NewsFeedViewController: TableViewImageDownloaderOutuput {
    func downloaded(_ image: UIImage, forCellAt indexPaths: Set<IndexPath>) {
        indexPaths.forEach { indexPath in
            guard let cell = tableView.cellForRow(at: indexPath) as? NewsArticleCell else {
                return
            }

            cell.upateImage(image)
        }
    }
}
