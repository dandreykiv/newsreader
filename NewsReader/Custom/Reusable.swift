import UIKit

protocol Reusable: class {
    static var identifier: String { get }
    static var nibName: String { get }
}

extension Reusable {
    static var identifier: String {
        return String(describing: self)
    }

    static var nibName: String {
        return identifier
    }
}

extension UITableViewCell: Reusable {}
