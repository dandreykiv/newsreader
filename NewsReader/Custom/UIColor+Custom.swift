import UIKit

extension UIColor {
    static func custom(_ hexString: String) -> UIColor? {
        guard let num = Int(hexString, radix: 16) else {
            return nil
        }

        return UIColor.from(hex: num)
    }

    static func from(hex: Int) -> UIColor {
        let red = Float((hex & 0xff0000) >> 16) / 255
        let green = Float((hex & 0x00ff00) >> 8) / 255
        let blue = Float(hex & 0x0000ff) / 255
        return UIColor(red: CGFloat(red), green: CGFloat(green), blue: CGFloat(blue), alpha: 1.0)
    }
}
