# Overview

News Reader sample app. The purpose of the app is to show how to work with newsapi.org.\
The application consisits of five different tabs, each tab presents news for a given category.
Available news gategories are `general`, `business`, `entertainment`, `science`, `sports`.\
The data is being transfered in JSON format.

## Project language

The project is entirely written on _Swift_ 4.2.

## Platforms supported

- iPhone

## Deployment target

- iOS 11 and up

## Installation

The [`cocoapods`](https://www.google.com) is being used as dependency manager in the project.

To install the CocoaPods dependencies:

```bash
pod install
```

Then use `NewsReader.xcworkspace`.

## Dependencies

- [`AlamofireImage`](https://github.com/Alamofire/AlamofireImage) - is being used to implement images lazy loading.
