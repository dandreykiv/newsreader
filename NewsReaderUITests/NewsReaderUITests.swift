import XCTest

class NewsReaderUITests: XCTestCase {

    override func setUp() {
        continueAfterFailure = false

        XCUIApplication().launch()
        XCUIDevice().orientation = .portrait
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTabBarHasFiveTabs() {
        let tabBarButtons = XCUIApplication().tabBars.buttons
        XCTAssertEqual(tabBarButtons.count, 5)
    }

    func testTabsExist() {
        let tabBarsQuery = XCUIApplication().tabBars
        XCTAssertTrue(tabBarsQuery.buttons[AccessibilityIdentifiers.generalTabIdentifier].exists)
        XCTAssertTrue(tabBarsQuery.buttons[AccessibilityIdentifiers.businessTabIdentifier].exists)
        XCTAssertTrue(tabBarsQuery.buttons[AccessibilityIdentifiers.entertainmentTabIdentifier].exists)
        XCTAssertTrue(tabBarsQuery.buttons[AccessibilityIdentifiers.scienceTabIdentifier].exists)
        XCTAssertTrue(tabBarsQuery.buttons[AccessibilityIdentifiers.sportsTabIdentifier].exists)
    }

    func testTabsHaveCorrectNames() {
        let tabBarsQuery = XCUIApplication().tabBars

        XCTAssertEqual(
            tabBarsQuery.buttons[AccessibilityIdentifiers.generalTabIdentifier].label,
            Category.general.name.capitalized
        )

        XCTAssertEqual(
            tabBarsQuery.buttons[AccessibilityIdentifiers.businessTabIdentifier].label,
            Category.business.name.capitalized
        )

        XCTAssertEqual(
            tabBarsQuery.buttons[AccessibilityIdentifiers.entertainmentTabIdentifier].label,
            Category.entertainment.name.capitalized
        )

        XCTAssertEqual(
            tabBarsQuery.buttons[AccessibilityIdentifiers.scienceTabIdentifier].label,
            Category.science.name.capitalized
        )

        XCTAssertEqual(
            tabBarsQuery.buttons[AccessibilityIdentifiers.sportsTabIdentifier].label,
            Category.sports.name.capitalized
        )
    }
}
